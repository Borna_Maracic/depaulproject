CREATE DATABASE  IF NOT EXISTS `depaulproject` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci */ /*!80016 DEFAULT ENCRYPTION='N' */;
USE `depaulproject`;
-- MySQL dump 10.13  Distrib 8.0.19, for Win64 (x86_64)
--
-- Host: localhost    Database: depaulproject
-- ------------------------------------------------------
-- Server version	8.0.19

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `beskucnici`
--

DROP TABLE IF EXISTS `beskucnici`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `beskucnici` (
  `IDBeskucnici` int NOT NULL,
  `Lokacija` varchar(45) DEFAULT NULL,
  `Neidentificirana osoba` varchar(45) DEFAULT NULL,
  `Spol` varchar(2) DEFAULT NULL,
  `Procjena dobi` int DEFAULT NULL,
  `Susreli u` varchar(45) DEFAULT NULL,
  `Mjesto noćenja` varchar(45) DEFAULT NULL,
  `Mjesto noćenja u Studenom` varchar(45) DEFAULT NULL,
  `Osobna iskaznica` varchar(45) DEFAULT NULL,
  `Nisu jeli topli obrok` varchar(45) DEFAULT NULL,
  `Opljačkan` varchar(45) DEFAULT NULL,
  `Fizički napadnut` varchar(45) DEFAULT NULL,
  `Zdravstveno stanje` varchar(45) DEFAULT NULL,
  `Boravište tijekom zime` varchar(45) DEFAULT NULL,
  `Mjesto odrastanja` varchar(45) DEFAULT NULL,
  `Ostanak bez doma` varchar(45) DEFAULT NULL,
  `Redoviti prihod` varchar(45) DEFAULT NULL,
  `Godina rođenja` varchar(45) DEFAULT NULL,
  `Inicijali` varchar(45) DEFAULT NULL,
  `Mjesto prebivališta` varchar(45) DEFAULT NULL,
  `Najpotrebnija pomoć` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`IDBeskucnici`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `beskucnici`
--

LOCK TABLES `beskucnici` WRITE;
/*!40000 ALTER TABLE `beskucnici` DISABLE KEYS */;
/*!40000 ALTER TABLE `beskucnici` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `datumi`
--

DROP TABLE IF EXISTS `datumi`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `datumi` (
  `IDDatum` int NOT NULL,
  `Datum` date DEFAULT NULL,
  PRIMARY KEY (`IDDatum`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `datumi`
--

LOCK TABLES `datumi` WRITE;
/*!40000 ALTER TABLE `datumi` DISABLE KEYS */;
/*!40000 ALTER TABLE `datumi` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `depaul_agr`
--

DROP TABLE IF EXISTS `depaul_agr`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `depaul_agr` (
  `ID` int NOT NULL,
  `IDDatum` int DEFAULT NULL,
  `IDKorisnik` int DEFAULT NULL,
  `IDUsluga` int DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `IDDatum_idx` (`IDDatum`),
  KEY `IDKorisnik_idx` (`IDKorisnik`),
  KEY `IDUsluga_idx` (`IDUsluga`),
  CONSTRAINT `IDDatum` FOREIGN KEY (`IDDatum`) REFERENCES `datumi` (`IDDatum`) ON DELETE RESTRICT ON UPDATE CASCADE,
  CONSTRAINT `IDKorisnik` FOREIGN KEY (`IDKorisnik`) REFERENCES `korisnici` (`IDKorisnik`) ON DELETE RESTRICT ON UPDATE CASCADE,
  CONSTRAINT `IDUsluga` FOREIGN KEY (`IDUsluga`) REFERENCES `usluge` (`IDUsluge`) ON DELETE RESTRICT ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `depaul_agr`
--

LOCK TABLES `depaul_agr` WRITE;
/*!40000 ALTER TABLE `depaul_agr` DISABLE KEYS */;
/*!40000 ALTER TABLE `depaul_agr` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `korisnici`
--

DROP TABLE IF EXISTS `korisnici`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `korisnici` (
  `IDKorisnik` int NOT NULL,
  `Ime` varchar(45) DEFAULT NULL,
  `Prezime` varchar(45) DEFAULT NULL,
  `Ime oca` varchar(45) DEFAULT NULL,
  `Ime majke` varchar(45) DEFAULT NULL,
  `Spol` varchar(1) DEFAULT NULL,
  `Datum rođenja` date DEFAULT NULL,
  `Mjesto rođenja` varchar(45) DEFAULT NULL,
  `Država rođenja` varchar(45) DEFAULT NULL,
  `Državljanstvo` varchar(45) DEFAULT NULL,
  `Nacionalnost` varchar(45) DEFAULT NULL,
  `OIB` int DEFAULT NULL,
  `Adresa` varchar(45) DEFAULT NULL,
  `Posljednja adresa` varchar(45) DEFAULT NULL,
  `Beskućnik` varchar(2) DEFAULT NULL,
  `Kategorija beskućništva` varchar(200) DEFAULT NULL,
  `Nužni smještaj` varchar(2) DEFAULT NULL,
  `Vlastiti stan ili soba` varchar(2) DEFAULT NULL,
  `Podstanar` varchar(2) DEFAULT NULL,
  `Drugo (stanovanje)` varchar(200) DEFAULT NULL,
  `Stručna sprema` varchar(3) DEFAULT NULL,
  `Zanimanje` varchar(100) DEFAULT NULL,
  `Životni i poslovni interesi` varchar(200) DEFAULT NULL,
  `Prihod (ukupno)` varchar(45) DEFAULT NULL,
  `ZMN` varchar(2) DEFAULT NULL,
  `Mirovina` varchar(45) DEFAULT NULL,
  `Druga primanja` varchar(45) DEFAULT NULL,
  `Analiza financijskih problema` varchar(200) DEFAULT NULL,
  `Bračno stanje i djeca (obiteljska anamneza)` varchar(200) DEFAULT NULL,
  `Kontakt osoba` varchar(45) DEFAULT NULL,
  `Psihofizičko zdravlje (bolesti, ljekovi, ovisnosti...)` varchar(200) DEFAULT NULL,
  `Kažnjavan` varchar(2) DEFAULT NULL,
  `Dodatne napomene (kažnjavan)` varchar(200) DEFAULT NULL,
  `Centar za socijanu skrb` varchar(200) DEFAULT NULL,
  `Vjeroispovjest` varchar(200) DEFAULT NULL,
  `Mobitel` varchar(45) DEFAULT NULL,
  `Dodatne napomene (mobitel)` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`IDKorisnik`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `korisnici`
--

LOCK TABLES `korisnici` WRITE;
/*!40000 ALTER TABLE `korisnici` DISABLE KEYS */;
/*!40000 ALTER TABLE `korisnici` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `usluge`
--

DROP TABLE IF EXISTS `usluge`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `usluge` (
  `IDUsluge` int NOT NULL,
  `Naziv usluge` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`IDUsluge`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `usluge`
--

LOCK TABLES `usluge` WRITE;
/*!40000 ALTER TABLE `usluge` DISABLE KEYS */;
/*!40000 ALTER TABLE `usluge` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-04-23 16:28:31
