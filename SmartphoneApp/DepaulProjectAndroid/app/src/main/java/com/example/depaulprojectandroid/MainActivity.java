package com.example.depaulprojectandroid;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Build;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

public class MainActivity extends AppCompatActivity {

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Spinner dropdownAktivnosti = findViewById(R.id.spinnerAktivnosti);
        String[] items = new String[]{"Dorucak","Rucak","Vecera", "brijanje","Tusiranje","Volonteri","Sve i Svasta","Zdravstvena Skrb",
            "Probacija","Hagioterapija","Grupa podrske","Pravna pomoc","Odjeca i obuca","Pranje robe","Sisanje","Ormari"};
        ArrayAdapter<String> adapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item, items);
        dropdownAktivnosti.setAdapter(adapter);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            dropdownAktivnosti.setTooltipText("Aktivnosti");
        }

    }
}
